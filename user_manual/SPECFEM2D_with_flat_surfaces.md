## SEM2D——基于二维多区域模型合成理论地震图

以`EXAMPLES`文件夹中的`simple_topography_and_also_a_simple_fluid_layer`为例

## 模型分层

首先需要指定模型的分层，注意参数设置`interfacesfile = ./interfaces.dat`，这里给出`interfaces.dat`的一个示例。

注意：

1.模型的左下角为坐标原点(0,0)，水平向右为x轴，垂直向上为z轴，对于水平界面，只需给定两点的(x,z)坐标，即可确定这一分层界面。  

2.在两个界面之间，要给出垂向上单位元的个数。
    
      
    # number of interfaces
    4
    # interface number 1(bottom of the mesh)
    # 确定界面所需点的个数，水平界面需2点
    2
    # 其中一个点的坐标
    0      0
    # 另外一个点的坐标
    200000 0
    #interface number 2
    2
    0      15000
    200000 15000
    #interface number 3
    2
    0      25000
    200000 25000
    #interface number 4(top of the mesh)
    2
    0      40000
    200000 40000
    # layer number 1 (bottom layer)
    # z方向上单位元的个数，界面之间的距离是15000m，所以单位元在z方向的长度为1000m。
     15
    # layer number 2
     10
    # layer number 3 (top layer)
     15

## 给定每层模型的信息

`nbmodels = 3`，有多少层就需要给定每一层的介质参数

    # 层数   介质类型   密度   P波速度   S波速度  0 0  Qkappa Qmu 0 0 0 0 0 0 
     1       1      2600.d0  5793.3d0  3191.d0  0 0  924    600 0 0 0 0 0 0
     2       1      2900.d0  6791.5d0  3889.d0  0 0  807    600 0 0 0 0 0 0
     3       1      3379.d0  7995.2d0  4387.3d0 0 0  935    600 0 0 0 0 0 0

`xmin = 0.d0`, `xmax = 200000.d0`, `nx = 200`, 给定了单位元在x方向上的个数，x方向在0-200km均匀地分为200个单位元

`nbregions = 3` 给定每层x方向，z方向单位元的个数

    #nxmin nxmax nzmin nzmax material_number
    #由于是从底部往地表分层模型，所以模型底部正常来讲是速度最大，所以material_number从3-2-1。
       1    200    1     15     3
       1    200    16    25     2
       1    200    26    40     1

## 设定弯曲界面

类似于设定水平界面，只需给出多个点的坐标即可，下方给出一个示例。

    # number of curved interfaces
     4
    # interface number 1 (bottom of the mesh)
     2
     0 0
     5000 0
    # interface number 2
     7
        0 1000
     1500 1100
     2000 1180
     2500 1200
     3000 1220
     3500 1170
     5000 1100
    # interface number 3
     9
        0 2000
      500 2000
     1000 1900
     1500 1847
     2000 1900
     2500 2000
     3000 2090
     3500 2020
     5000 2000
    # interface number 4 (topography, top of the mesh)
     8
        0 3000
      500 3000
     1000 3100
     1500 3350
     2000 3250
     2500 3180
     3000 3090
     5000 3000
    #
    # layer number 1 (bottom layer)
     20
    # layer number 2
     20
    # layer number 3 (top layer)
     20

## 区域内含有速度异常体

需要给定该区域在x方向、z方向的单位元的个数，下方给出一个示例。

`nbmodels = 4`

    1 1 2700.d0 3000.d0 1732.051d0 0 0 9999 9999 0 0 0 0 0 0
    2 1 2500.d0 2700.d0 0 0 0 9999 9999 0 0 0 0 0 0
    3 1 2200.d0 2500.d0 1443.375d0 0 0 9999 9999 0 0 0 0 0 0
    4 1 2200.d0 2200.d0 1343.375d0 0 0 9999 9999 0 0 0 0 0 0
    
`nbregions = 4`

    # format of each line: nxmin nxmax nzmin nzmax material_number
    1 80  1 20 1
    1 80 21 40 2
    1 80 41 60 3
    60 70 21 40 4
